0.19.3
======
* add plot pulls channel from plot menu entry (#333)

0.19.2
======
* fix online freeze, range reset when adding channels

0.19.1
======
* fix cursor positions in log mode

0.19.0
======
* fix reset-on-channel-add bug, improve channel add/remove behavior
* improve channel edit interface, add ability to cancel changes
* break out channel list into separate window
* better channel list filtering with globs
* improved cursor control, add time cursors to all plots
* option to disable cursor labels
* online mode locks T=0 to right axis


0.18.0
======
* template option to set grid line alpha
* fix editing channel labels and units in channel config dialog
